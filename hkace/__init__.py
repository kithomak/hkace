# -*- coding: utf-8 -*-

"""Top-level package for hkace."""

__author__ = """Kit-Ho Mak"""
__email__ = 'kit.ho@findsolutiongroup.com'
__version__ = '0.1.0'
