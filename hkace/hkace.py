from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta

import os
import pandas as pd
pd.set_option("display.max_columns", 20)
# import numpy as np
import pymysql


# global settings for db
host = "starplatform.c2ueg0anncqg.ap-southeast-1.rds.amazonaws.com"
user = "starplatform"
password = "star2016"
port = 3306
curriculum_db = "teacher-service"
curriculum_conn = pymysql.connect(host, user=user, port=port, passwd=password, db=curriculum_db)

auth_db = "auth"
auth_conn = pymysql.connect(host, user=user, port=port, passwd=password, db=auth_db)

school_db = "fsschool"
school_conn = pymysql.connect(host, user=user, port=port, passwd=password, db=school_db)


def duration_to_str(duration):
    """
    Turn a duration (in seconds) into a day string, of the form `x days y hours z minutes`.

    :param float duration: duration in minutes
    :return: a day string as specified.
    :rtype: str
    """
    num_days, duration = divmod(duration, 24 * 60)
    num_hours, duration = divmod(duration, 60)
    num_mins, duration = divmod(duration, 60)
    day_str = ""
    if num_days > 0:
        day_str += "{:d} days ".format(int(num_days))
    if num_hours > 0 or num_days > 0:
        day_str += "{:d} hrs ".format(int(num_hours))
    if num_mins > 0 or num_hours > 0 or num_days > 0:
        day_str += "{:d} mins ".format(int(num_mins))
    day_str += "{:.2f} secs".format(duration)
    return day_str


if __name__ == "__main__":
    # school to report
    DOMAIN = "hkace"
    school_id = 155  # can be found on the database

    # FOR TESTING PURPOSE: do we include demo students?
    INCLUDE_DEMO_STUDENTS = False

    query = """
        SELECT 
            SUBSTRING_INDEX(profile.user,".",1) as school,
            profile.user as user,
            attempt.question_id as question_id,
            question.difficulty as difficulty,
            attempt.result as correct,
            attempt.start as start_time,
            attempt.end as end_time,
            attempt.mode,
            ccc.name_en as concept
        FROM students_attempt attempt
        INNER JOIN students_knowledgecomponent kc ON kc.id = attempt.kc_id
        INNER JOIN curriculum_cache_concept ccc ON kc.concept_id = ccc.id
        INNER JOIN students_profile profile ON kc.profile_id = profile.id
        INNER JOIN curriculum_cache_question question ON question.id = attempt.question_id
        WHERE SUBSTRING_INDEX(profile.user,".",1) = '{}'
        """.format(DOMAIN)
    df1 = pd.read_sql(query, con=curriculum_conn)
    df1 = df1.sort_values("start_time").reset_index(drop=True)
    df1["duration"] = (df1["end_time"] - df1["start_time"]).map(lambda x: x.total_seconds())

    # remove demo users
    if not INCLUDE_DEMO_STUDENTS:
        df1 = df1[~df1["user"].str.contains("demo") & ~df1["user"].str.contains("test")]

    # print(df1)

    query2 = """
    SELECT
        SUBSTRING_INDEX(users.login_name,".",1) as school,
        users.school_id,
        users.id as user_id,
        users.login_name as user,
        /* users.uuid, */
        user_roles.role
    FROM users
    INNER JOIN user_roles ON users.id = user_roles.user_id
    WHERE SUBSTRING_INDEX(users.login_name,".",1) = '{}'
    AND user_roles.role = 'student'
    """.format(DOMAIN)
    df2 = pd.read_sql(query2, con=school_conn)

    # remove demo users
    if not INCLUDE_DEMO_STUDENTS:
        df2 = df2[~df2["user"].str.contains("demo") & ~df2["user"].str.contains("test")]
    # print(df2)

    # school_id = df2["school_id"][0]

    query3 = """
    SELECT 
        school_classes.school_id,
        school_classes.id as class_id,
        school_classes.display_name as class_name,
        student_classes.user_id
    FROM school_classes
    RIGHT JOIN student_classes ON school_classes.id = student_classes.school_class_id
    WHERE school_id = {}
    """.format(school_id)
    df3 = pd.read_sql(query3, con=school_conn)
    df3 = df3[df3["user_id"].isin(set(df2["user_id"]))]
    # print(df3)

    attempt_df = df1.merge(df2.merge(df3))
    attempt_df = attempt_df.drop(["school_id", "user_id", "role", "class_id"], axis=1)
    attempt_df["user"] = attempt_df["user"].map(lambda x: ".".join(x.split(".")[1:]))
    print(attempt_df)

    class_df = attempt_df.groupby("class_name").agg({
        "user": "nunique",
        "question_id": "count",
        "duration": "sum"
    })
    class_df = class_df.merge(df3.groupby("class_name").count()["school_id"],
                              left_on="class_name", right_on="class_name", how="right").fillna(0)
    class_df = class_df.sort_index()
    class_df = class_df.rename(columns={
        "school_id": "total_students",
        "user": "num_active_students",
        "question_id": "num_attempts",
    })
    class_df = class_df[["total_students", "num_active_students", "num_attempts", "duration"]]
    class_df["duration"] = class_df["duration"].map(duration_to_str)

    # rename the columns into human friendly names for export
    class_df = class_df.rename(columns={
        "total_students": "# Total Students",
        "num_active_students": "# Attempted Students",
        "num_attempts": "# Total Attempts",
        "duration": "Duration"
    })
    class_df.index.name = "School"

    print(class_df)

    writer = pd.ExcelWriter('reports/{}_{}_report.xlsx'.format(datetime.today().date(), DOMAIN), engine='xlsxwriter')
    workbook = writer.book

    # write school reports
    if not os.path.isdir("./reports"):
        os.mkdir("./reports")
    class_df.to_excel(writer, sheet_name="School Report")

    writer.sheets['School Report'].set_column(0, 4, 18)

    student_df = attempt_df.groupby("user").agg({
        "start_time": "min",
        "end_time": "max",
        "correct": "mean",
        "duration": "sum",
        "difficulty": "count",
        "concept": lambda x: x.tail(1)
    })
    student_df = student_df.rename(columns={
        "start_time": "first_login_time",
        "end_time": "last_login_time",
        "correct": "correct_rate",
        "difficulty": "num_attempts",
        "concept": "last_concept"
    })
    student_df["duration"] = student_df["duration"].map(duration_to_str)
    # student_df["correct_rate"] = student_df["correct_rate"].map(lambda x: "{:.2f}%".format(x * 100))

    # rename the columns into human friendly names for export
    student_df = student_df.rename(columns={
        "first_login_time": "First Login Time",
        "last_login_time": "Last Login Time",
        "correct_rate": "Correct Rate",
        "duration": "Duration",
        "num_attempts": "# Attempts",
        "last_concept": "Last Concept"
    })
    student_df.index.name = "User"

    print(student_df)

    student_df.to_excel(writer, sheet_name="Student Report")

    percentage_format = workbook.add_format({'num_format': '0.00%'})
    writer.sheets['Student Report'].set_column(0, 6, 18)
    writer.sheets['Student Report'].set_column(3, 3, 18, percentage_format)

    writer.save()


